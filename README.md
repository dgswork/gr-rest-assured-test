# Example that shows how to test some github api resources

## Introduction

In order to properly test everything you need java 1.8 or higher. All tests were executed and verified under Oracle Java JDK 1.8.
This is just a code shown as an example and not intended to be used on any real application. Please, if so, do it at your own risk.
The tests cover the creation of some gists, access them and delete them. Moreover, note the tests are dependent on each other.  

## HOW-TO

- Get an OAUTH2 token to access the github api with proper permissions. Follow this guide : https://help.github.com/articles/creating-a-personal-access-token-for-the-command-line/
- Once completed, access the file with name, **GistTests.java**, and change the provider method to fit your needs using your Github API Token:

```
    /**
     * Provider for creating a gist. Please, add new cases as needed and modify the related tests
     *
     * @return Object[][]
     */
    @DataProvider
    public Object[][] createProvider() {
        return new Object[][]{
                // Right token
                new Object[]{
                    // Token. Change this for one of your tokens
                    "WRITE HERE A VALID TOKEN",
                    // Content to send or receive
                    "{\"description\": \"Testing Gist Creation\",\"public\": \"false\", \"files\": {\"test-system.txt\":{\"content\":\"Testing creating GIST\"}}}"
                },
                // Bad Token
                new Object[]{
                    "bad-token",
                    "Bad credentials"
                }
        };
    }
```

Add as many new cases as you need in this provider for testing purposes and take into consideration modify the related tests if required 

- Execute the tests via maven : **mvn test** 

Among the displayed result, you should find something like the following lines : 

```
    -------------------------------------------------------
     T E S T S
    -------------------------------------------------------
    Running TestSuite
    Oct 03, 2018 1:15:13 AM net.sxgio.example.GistTests createTest
    INFO: Creating a new GIST
    Oct 03, 2018 1:15:18 AM net.sxgio.example.GistTests createTest
    INFO: Creating a new GIST
    Oct 03, 2018 1:15:20 AM net.sxgio.example.GistTests lambda$readTest$0
    INFO: Accessing gist with code: 89a81d06a5b3ee9623424fd94eb51122 responded: 200
    Oct 03, 2018 1:15:21 AM net.sxgio.example.GistTests lambda$deleteTest$1
    INFO: Deleting gist with code: 89a81d06a5b3ee9623424fd94eb51122 responded: 204
    Oct 03, 2018 1:15:21 AM net.sxgio.example.GistTests lambda$deleteTest$1
    INFO: Accessing a deleted gist with code: 89a81d06a5b3ee9623424fd94eb51122 responded: 404
    Tests run: 4, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 8.341 sec
    
    Results :
    
    Tests run: 4, Failures: 0, Errors: 0, Skipped: 0
    
    [INFO] ------------------------------------------------------------------------
    [INFO] BUILD SUCCESS
    [INFO] ------------------------------------------------------------------------
    [INFO] Total time: 12.438 s
    [INFO] Finished at: 2018-10-03T01:15:22+02:00
    [INFO] Final Memory: 16M/214M
    [INFO] ------------------------------------------------------------------------
```

- Surefire reports are also created after the **mvn test** execution and they could be seen in the folder **target/surefire-reports**. 

## Contributors

*Sergio Martín <sergio.martin@sxgio.net>* 

## License

© 2018 SXGIO.net. 
