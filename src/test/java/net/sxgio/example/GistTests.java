/**
 * <strong>Name:  GistTests.java</strong><br>
 * <strong>Desc:  Tester for some gist operations</strong><br>
 *
 * @package net.sxgio.example
 * @author Sergio Martin, sergio.martin@sxgio.net
 * @copyright 2018 SXGIO.net
 * @license Apache 2 License http://www.apache.org/licenses/LICENSE-2.0.html
 * @link https://www.sxgio.net
 * @since 0.1.0
 */
package net.sxgio.example;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import io.restassured.path.json.JsonPath;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.hamcrest.Matchers;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.logging.Logger;

/**
 * GistTests class
 *
 * @author Sergio Martin sergio.martin@sxgio.net
 * @version %I%, %G%
 * @since 0.1.0
 */
public class GistTests {

    /**
     * accept header as requested by the api.
     */
    private final String ACCEPT_HEADER = "application/vnd.github.v3+json";

    /**
     * Common logger to know what is happening as the tests are being executed
     */
    private Logger logger = Logger.getLogger(this.getClass().getName());

    /**
     * URI to test. Perhaps it should be better use DI
     */
    private static final String ROOT_URI = "https://api.github.com";

    /**
     * Gist identifiers will be kept into a HashMap<gistId,token>
     */
    private static HashMap<String, String> gists = new HashMap<>();

    /**
     * HTTP expected response code when creating a new gist
     */
    private static final int HTTP_CREATED = 201;

    /**
     * HTTP expected response code when a bad token is sent
     */
    private static final int HTTP_FORBIDDEN = 401;

    /**
     * HTTP expected response code when erasing a gist
     */
    private static final int HTTP_NO_CONTENT = 204;

    /**
     * HTTP expected response code when getting a gist
     */
    private static final int HTTP_OK = 200;

    /**
     * HTTP expected response code when an element was not found
     */
    private static final int HTTP_NOT_FOUND = 404;

    /**
     * Provider with info to create new gists. Please, add new cases as needed and modify the related tests
     *
     * @return Object[][]
     */
    @DataProvider
    public Object[][] createProvider() {
        return new Object[][]{
                // Right token. Add new cases with other JSONs
                new Object[]{
                    // Token. Change this for a valid token.
                    "WRITE HERE A VALID TOKEN",
                    // Content to send or receive.
                    "{\"description\": \"Testing Gist Creation\",\"public\": \"false\", \"files\": {\"test-system.txt\":{\"content\":\"Testing creating GIST\"}}}"
                },
                // Bad Token. Last string is what is expected within the response
                new Object[]{
                    "bad-token",
                    "Bad credentials"
                }
        };
    }

    /**
     * Test to check the creation of a new gist.
     *
     * @param token   OAUTH2 token (check github configuration)
     * @param content Content to send or receive
     *
     */
    @Test(dataProvider = "createProvider")
    public void createTest(String token, String content) {
        // Logging Info
        logger.info("Creating a new GIST");
        Response response = given()
            .contentType(ContentType.JSON)
            .accept(ACCEPT_HEADER)
            .body(content)
            .auth().oauth2(token)
        .when()
            .post(ROOT_URI+"/gists");

        // Add more responses if needed or define a strategy pattern.
        if (response.statusCode()== HTTP_FORBIDDEN) {
            response.then().body(Matchers.containsString(content));
        } else {
            // Matching what expected. Add anything that needs to be checked
            response.then()
                .statusCode(HTTP_CREATED)
                .body("id", Matchers.instanceOf(String.class))
                .body("public", Matchers.equalTo(false))
                .body("comments", equalTo(0));
            // Analyse the JSON to take out the gist identifier
            JsonPath jsonResponse = response.jsonPath();
            // Getting id and token and push them into a hash map to be accessible by other tests
            gists.put(jsonResponse.get("id"), token);
        }
    }

    /**
     * Test to check if the new gists previously created are available
     * NOTE: Just only searching by id. Perhaps another tests are needed
     * more customized with other options for a more advanced way of searching
     */
    @Test(dependsOnMethods = {"createTest"})
    public void readTest() {
        gists.forEach((id, token)->{
            Response response = given()
                .contentType(ContentType.JSON)
                .accept(ACCEPT_HEADER)
                .auth().oauth2(token)
            .when()
                .get(ROOT_URI+"/gists/"+id);
            logger.info("Accessing gist with code: "+id+ " replied: "+response.getStatusCode());
            response.then().statusCode(HTTP_OK);
            response.then().body("id", equalTo(id));
        });
    }

    /**
     * Test to check the deletion of a gist.
     */
    @Test(dependsOnMethods = {"readTest"})
    public void deleteTest() {
        gists.forEach((id, token)->{
            Response response = given()
                .contentType(ContentType.JSON)
                .accept(ACCEPT_HEADER)
                .auth().oauth2(token)
            .when()
                .delete(ROOT_URI+"/gists/"+id);
            logger.info("Deleting gist with code: "+id+ " replied: "+response.getStatusCode());
            response.then().statusCode(HTTP_NO_CONTENT);
            // check if element was deleted
            response = given()
                .contentType(ContentType.JSON)
                .accept(ACCEPT_HEADER)
                .auth().oauth2(token)
            .when()
                .get(ROOT_URI+"/gists/"+id);
            logger.info("Accessing a deleted gist with code: "+id+ " replied: "+response.getStatusCode());
            response.then().statusCode(HTTP_NOT_FOUND);
        });
    }
}
